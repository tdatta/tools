#!/usr/bin/python
import sys
#from BeautifulSoup import BeautifulSoup
from bs4 import BeautifulSoup
from string import Template

# 0 = Tag value
# 1 = Field name
# 3 = Data Type
# 5 = Description

''' 
#FixMap{
messages =>{
    {message_name} => #{
            "Category" => {1}
            ,"type" => {1}
            ,"fields_pl" => {1}
            ,"components_pl" => {2}
            }
}
, fields =>{

            "Number" => {1}
            ,"Name" => {1}
            ,"Type" => {1}
}
, components => {

            "Name" => {1}
            ,"Fields" => {1}
            ,"Groups" => {1}
            ,"Components" => {1}
}
, groups => {
              fields => []
              ,components=>[]
              ,groups=>[]
}.
'''
ERL_FIX_Map = Template('''-module($ModuleName).
-export([messages/0,
         fields/0,
         components/0,
         groups/0,
         header/0,
         trailer/0]).

messages() ->
 #{$MsgList}.
fields() ->
#{$FldList}.
components() ->
 #{$CmpList}.
groups() ->
 #{$GrpList}.
header()->
ok.
trailer()->
ok.
''')

GrpTmp = Template('''
                              "$GrpName" => #{
                              "Fields" => $F
                              ,"Groups" => $G
                              ,"Components" => $Cmp
}
''')
MsgTmp = Template('''"$MsgName" => #{
                              "Category" => "$C"
                              ,"Type" => "$T"
                              ,"Fields" => $F
                              ,"Components" => $Cmp
}
,"$T" => #{"Category"=>"$C"
           ,"Name" => "$MsgName"}
''')
MsgFld = Template('''"$N" =>#{"Required" => "$R", "Sequence" => undefined}''')

FldTmp = Template('''
"$Name" => #{"Number" => "$N" ,"Type" => "$T" ,"ValidValues" =>$V}
, "$N" => #{"Name"=>"$Name" ,"Type"=>"$T" ,"ValidValues"=>$V}
''')

CmpTmp = Template('''"$N" => #{
                              "Fields" => $F
                              ,"Groups" => $G
                              ,"Components" => $C}''')

ListTmp = Template("#{$List}")
PLTmp = Template('{"$Value", "$Desc"}')


def get_all_fields(row):
    fl = row.findAll('field')
    AllFields =''
    for f in fl:
        AllFields += MsgFld.substitute(N=f['name'],
                                       R=f['required']) + "\n,"

    return ListTmp.substitute(List=AllFields[:-2])
    

def get_all_component(row):
    fl = row.findAll('component')
    AllFields =''
    for f in fl:
        AllFields += MsgFld.substitute(N=f['name'],
                                       R=f['required']) + "\n,"

    return ListTmp.substitute(List=AllFields[:-1])


def get_messages(soup):
    messages = soup.find("messages")
    AllMsg = ''
    for row in messages.findAll("message"):
        msgName = row['name']
        category = row['msgcat']
        types  = row['msgtype']
        fields = get_all_fields(row)
        components = get_all_component(row)
        AllMsg += MsgTmp.substitute(MsgName=msgName,
                                        C=category,
                                        T=types,
                                        F=fields,
                                        Cmp=components) + "\n,"
    return AllMsg[:-1]

def get_fields(soup):
    fields = soup.find('fields')
    AllFields = ''
    for fld in fields.findAll("field"):
        number = fld['number']
        name = fld['name']
        types = fld['type']
        valid_values = fld.findAll("value")
        value_des = [PLTmp.substitute(Value = str(x['enum']),  Desc=str(x['description'])) for x in valid_values]
        value_des = "[" + ",".join(value_des) + "]"
        AllFields += FldTmp.substitute(N=number,
                                       Name=name,
                                       T=types,
                                        V=value_des) + "\n,"
    return AllFields[:-1]

def get_components(soup):
    components = soup.find('components')
    # x = components.findAll("component")
    # y = components.findAll("component", recursive=False)

    # import pdb
    # pdb.set_trace()
    AllComponents = ''
    for c in components.findAll("component"):
        if c.text <> '':
            [fields, groups, components]= get_component(c)
            name = c['name']
            AllComponents += CmpTmp.substitute(N=name,
                                                F=fields,
                                                G=groups,
                                                C=components) + "\n,"
        else:
            continue
    return AllComponents[:-1]


def get_groups(component):
    grplst = component.findAll("group", recursive=False)
    # import pdb
    # pdb.set_trace()
    AllGroups = ''
    for grp in grplst:
        fldlst = grp.findAll("field")
        fldlst = map(lambda x: '"{}"'.format(x['name']), fldlst)
        AllFields = "[{}]".format(",".join(fldlst))
        grplst = grp.findAll("group")
        grplst = map(lambda x: '"{}"'.format(x['name']), grplst)
        AllGrp = "[{}]".format(",".join(grplst))
        cmplst = grp.findAll("component")
        cmplst = map(lambda x: '"{}"'.format(x['name']), cmplst)
        AllComponents = "[{}]".format(",".join(cmplst))

                              # "$GrpName" => #{
                              # "Fields" => $F
                              # ,"Groups" => $G
                              # ,"Components" => $Cmpy
        AllGroups += GrpTmp.substitute(GrpName=grp['name']
                                       ,F=AllFields
                                       ,G=AllGrp
                                       ,Cmp=AllComponents
                                       ) + "\n,"
    return AllGroups[:-1]

def get_component(component):

    fldlst = component.findAll("field", recursive=False)
    fldlst = map(lambda x: '"{}"'.format(x['name']), fldlst)
    AllFields = "[{}]".format(",".join(fldlst))

    grplst = component.findAll("group", recursive=False)
    grplst = map(lambda x: '"{}"'.format(x['name']), grplst)
    AllGroups = "[{}]".format(",".join(grplst))
    cmplst = component.findAll("component", recursive=False)
    cmplst = map(lambda x: '"{}"'.format(x['name']), cmplst)
    AllComponents = "[{}]".format(",".join(cmplst))

    return [AllFields, AllGroups, AllComponents]


if __name__ == '__main__':
    src = sys.argv[1] 
    dest = sys.argv[2] 
    module_name = dest.split(".")[0].lower()
    # BeautifulSoup.NESTABLE_TAGS['component']= ['component']
    # BeautifulSoup.RESET_NESTING_TAGS.has_key('component')
    soup = BeautifulSoup(open(src, 'r').read(), 'html.parser')
    # soup = BeautifulSoup(open(src, 'r').read())
    table = soup.find("table", { "class" : "ListTable" })

    MsgList = ''
    FldList = ''
    CmpList = ''
    MsgList=get_messages(soup)[:-1]
    MsgList  = MsgList.encode("utf8")

    FldList = get_fields(soup)
    FldList = FldList.encode("utf8")

    CmpList = get_components(soup)
    CmpList = CmpList.encode("utf8")

    GrpList = get_groups(soup)
    GrpList = GrpList.encode("utf8")
    with open(dest, 'w') as destfile:
        destfile.write(ERL_FIX_Map.substitute(ModuleName=module_name,
                                                  MsgList=MsgList,
                                                  CmpList=CmpList,
                                                  FldList=FldList,
                                                  GrpList=GrpList))
    print "done"
